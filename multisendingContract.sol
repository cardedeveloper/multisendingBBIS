pragma solidity 0.4.20;



contract BBIInterface {
    function transfer(address _to, uint256 _value) external view returns (bool) ;
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool);
    //event Transfer(address indexed from, address indexed to, uint256 value);
}


contract distribuiteTokens {

    address BBIAddress; //Address of smart contract BBI
    BBIInterface BBIContract; // Interface
    
    /*blockchain developers*/
    address oscar= 0x4d9661994F96d70BC7425887A2aE820F176e8d41; 
    address saravana=0xca35b7d915458ef540ade6068dfe2f44e8fa733c; 
    
    mapping (address => bool) internal managers;
    
    function distribuiteTokens(address _BBIAddress) public{
        BBIAddress = _BBIAddress;
        BBIContract = BBIInterface(BBIAddress); //initializa the Interface
        
        //make managers the developers
        managers[oscar]=true;
        managers[saravana]= true;
        
    }
    
    
     modifier onlyManagers() {
        // only presale manager
        require(managers[msg.sender]);
        _;
    }
    
    modifier onlyDevelopers(){
        //only blockchain developers
        require(msg.sender== saravana || msg.sender== oscar);
        _;
    }
    
    
    
    //add  anew presale manager
    function addManager(address _newManager) public onlyDevelopers{
        managers[_newManager]=true;
    }
    
    // see a manager status
    function isManager(address _seeManager) public constant returns(bool _isManager){
        return managers[_seeManager];
    }
    
    //withdrawl bbis
    function returnBBIS(address _to, uint256 _qty)public onlyDevelopers{
         BBIContract.transfer(_to, _qty);
    }
    
    //make multiple multiTransactions in one block
    function multiTransfer(address[] _owners, uint256[] _bbis) public onlyManagers  returns(bool _success){
        require(_owners.length == _bbis.length && _owners.length >0);
        uint size =_owners.length;
        for(uint i=0;i<size;i++ ){
             BBIContract.transfer(_owners[i], _bbis[i]);
        }
        
        return true;
    }
}